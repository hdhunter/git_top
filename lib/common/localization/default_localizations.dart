import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:git_top_flutter/common/localization/gsy_string_base.dart';
import 'package:git_top_flutter/common/localization/gsy_string_en.dart';
import 'package:git_top_flutter/common/localization/gsy_string_zh.dart';

///自定义多语言实现
class TopLocalizations {
  final Locale locale;

  TopLocalizations(this.locale);

  ///根据不同 locale.languageCode 加载不同语言对应
  ///TopStringEn和TopStringZh都继承了TopStringBase
  static Map<String, TopStringBase> _localizedValues = {
    'en': new TopStringEn(),
    'zh': new TopStringZh(),
  };

  TopStringBase get currentLocalized {
    if (_localizedValues.containsKey(locale.languageCode)) {
      return _localizedValues[locale.languageCode];
    }
    return _localizedValues["en"];
  }

  ///通过 Localizations 加载当前的 TopLocalizations
  ///获取对应的 TopStringBase
  static TopLocalizations of(BuildContext context) {
    return Localizations.of(context, TopLocalizations);
  }

  ///通过 Localizations 加载当前的 TopLocalizations
  ///获取对应的 TopStringBase
  static TopStringBase i18n(BuildContext context) {
    return (Localizations.of(context, TopLocalizations) as TopLocalizations)
        .currentLocalized;
  }
}
