import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:git_top_flutter/common/localization/default_localizations.dart';

/**
 * 多语言代理
 * Created by HunterZhang
 * Date: 2019-12-15
 */
class TopLocalizationsDelegate extends LocalizationsDelegate<TopLocalizations> {

  TopLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    ///支持中文和英语
    return true;
  }

  ///根据locale，创建一个对象用于提供当前locale下的文本显示
  @override
  Future<TopLocalizations> load(Locale locale) {
    return new SynchronousFuture<TopLocalizations>(new TopLocalizations(locale));
  }

  @override
  bool shouldReload(LocalizationsDelegate<TopLocalizations> old) {
    return false;
  }

  ///全局静态的代理
  static TopLocalizationsDelegate delegate = new TopLocalizationsDelegate();
}
