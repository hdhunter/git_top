/**
 * 网络结果数据
 * Created by HunterZhang
 * Date: 2019-12-16
 */
class ResultData {
  var data;
  bool result;
  int code;
  var headers;

  ResultData(this.data, this.result, this.code, {this.headers});
}
