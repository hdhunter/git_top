/**
 * Created by HunterZhang
 * Date: 2019-12-16
 */

class HttpErrorEvent {
  final int code;

  final String message;

  HttpErrorEvent(this.code, this.message);
}
