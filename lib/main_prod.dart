import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:git_top_flutter/app.dart';
import 'package:git_top_flutter/env/config_wrapper.dart';
import 'package:git_top_flutter/env/env_config.dart';
import 'package:git_top_flutter/page/error_page.dart';

import 'env/prod.dart';

void main() {
  runZoned(() {
    ErrorWidget.builder = (FlutterErrorDetails details) {
      Zone.current.handleUncaughtError(details.exception, details.stack);
      return ErrorPage(
          details.exception.toString() + "\n " + details.stack.toString(),
          details);
    };
    runApp(ConfigWrapper(
      child: FlutterReduxApp(),
      config: EnvConfig.fromJson(config),
    ));
  }, onError: (Object obj, StackTrace stack) {
    ///do not thing
  });
}
