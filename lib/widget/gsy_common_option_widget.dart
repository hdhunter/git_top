import 'package:flutter/material.dart';
import 'package:git_top_flutter/common/localization/default_localizations.dart';
import 'package:git_top_flutter/common/style/gsy_style.dart';
import 'package:git_top_flutter/common/utils/common_utils.dart';
import 'package:share/share.dart';

/**
 * Created by HunterZhang
 * Date: 2019-12-26
 */
class TopCommonOptionWidget extends StatelessWidget {
  final List<TopOptionModel> otherList;

  final String url;

  TopCommonOptionWidget({this.otherList, String url})
      : this.url = (url == null) ? TopConstant.app_default_share_url : url;

  _renderHeaderPopItem(List<TopOptionModel> list) {
    return new PopupMenuButton<TopOptionModel>(
      child: new Icon(TopICons.MORE),
      onSelected: (model) {
        model.selected(model);
      },
      itemBuilder: (BuildContext context) {
        return _renderHeaderPopItemChild(list);
      },
    );
  }

  _renderHeaderPopItemChild(List<TopOptionModel> data) {
    List<PopupMenuEntry<TopOptionModel>> list = new List();
    for (TopOptionModel item in data) {
      list.add(PopupMenuItem<TopOptionModel>(
        value: item,
        child: new Text(item.name),
      ));
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    List<TopOptionModel> constList = [
      new TopOptionModel(TopLocalizations.i18n(context).option_web,
          TopLocalizations.i18n(context).option_web, (model) {
        CommonUtils.launchOutURL(url, context);
      }),
      new TopOptionModel(TopLocalizations.i18n(context).option_copy,
          TopLocalizations.i18n(context).option_copy, (model) {
        CommonUtils.copy(url ?? "", context);
      }),
      new TopOptionModel(TopLocalizations.i18n(context).option_share,
          TopLocalizations.i18n(context).option_share, (model) {
        Share.share(
            TopLocalizations.i18n(context).option_share_title + url ?? "");
      }),
    ];
    var list = [...constList, ...?otherList];
    return _renderHeaderPopItem(list);
  }
}

class TopOptionModel {
  final String name;
  final String value;
  final PopupMenuItemSelected<TopOptionModel> selected;

  TopOptionModel(this.name, this.value, this.selected);
}
