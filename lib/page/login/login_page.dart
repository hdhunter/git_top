import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:git_top_flutter/common/config/config.dart';
import 'package:git_top_flutter/common/local/local_storage.dart';
import 'package:git_top_flutter/common/localization/default_localizations.dart';
import 'package:git_top_flutter/redux/gsy_state.dart';
import 'package:git_top_flutter/redux/login_redux.dart';
import 'package:git_top_flutter/common/style/gsy_style.dart';
import 'package:git_top_flutter/common/utils/common_utils.dart';
import 'package:git_top_flutter/widget/gsy_flex_button.dart';
import 'package:git_top_flutter/widget/gsy_input_widget.dart';

/**
 * 登录页
 * Created by HunterZhang
 * Date: 2019-12-16
 */
class LoginPage extends StatefulWidget {
  static final String sName = "login";

  @override
  State createState() {
    return new _LoginPageState();
  }
}

class _LoginPageState extends State<LoginPage> with LoginBLoC {
  @override
  Widget build(BuildContext context) {
    /// 触摸收起键盘
    return new GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
        body: new Container(
          color: Theme.of(context).primaryColor,
          child: new Center(
            ///防止overFlow的现象
            child: SafeArea(
              ///同时弹出键盘不遮挡
              child: SingleChildScrollView(
                child: new Card(
                  elevation: 5.0,
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  color: TopColors.cardWhite,
                  margin: const EdgeInsets.only(left: 30.0, right: 30.0),
                  child: new Padding(
                    padding: new EdgeInsets.only(
                        left: 30.0, top: 40.0, right: 30.0, bottom: 0.0),
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        new Image(
                            image: new AssetImage(TopICons.DEFAULT_USER_ICON),
                            width: 90.0,
                            height: 90.0),
                        new Padding(padding: new EdgeInsets.all(10.0)),
                        new TopInputWidget(
                          hintText: TopLocalizations.i18n(context)
                              .login_username_hint_text,
                          iconData: TopICons.LOGIN_USER,
                          onChanged: (String value) {
                            _userName = value;
                          },
                          controller: userController,
                        ),
                        new Padding(padding: new EdgeInsets.all(10.0)),
                        new TopInputWidget(
                          hintText: TopLocalizations.i18n(context)
                              .login_password_hint_text,
                          iconData: TopICons.LOGIN_PW,
                          obscureText: true,
                          onChanged: (String value) {
                            _password = value;
                          },
                          controller: pwController,
                        ),
                        new Padding(padding: new EdgeInsets.all(30.0)),
                        new TopFlexButton(
                          text: TopLocalizations.i18n(context).login_text,
                          color: Theme.of(context).primaryColor,
                          textColor: TopColors.textWhite,
                          onPress: loginIn,
                        ),
                        new Padding(padding: new EdgeInsets.all(15.0)),
                        InkWell(
                          onTap: () {
                            CommonUtils.showLanguageDialog(context);
                          },
                          child: Text(
                            TopLocalizations.i18n(context).switch_language,
                            style: TextStyle(color: TopColors.subTextColor),
                          ),
                        ),
                        new Padding(padding: new EdgeInsets.all(15.0)),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

mixin  LoginBLoC on State<LoginPage> {
  final TextEditingController userController = new TextEditingController();

  final TextEditingController pwController = new TextEditingController();

  var _userName = "";

  var _password = "";

  @override
  void initState() {
    super.initState();
    initParams();
  }

  @override
  void dispose() {
    super.dispose();
    userController.removeListener(_usernameChange);
    pwController.removeListener(_passwordChange);
  }

  _usernameChange() {
    _userName = userController.text;
  }

  _passwordChange() {
    _password = pwController.text;
  }

  initParams() async {
    _userName = await LocalStorage.get(Config.USER_NAME_KEY);
    _password = await LocalStorage.get(Config.PW_KEY);
    userController.value = new TextEditingValue(text: _userName ?? "");
    pwController.value = new TextEditingValue(text: _password ?? "");
  }

  loginIn() async {
    if (_userName == null || _userName.isEmpty) {
      return;
    }
    if (_password == null || _password.isEmpty) {
      return;
    }

    ///通过 redux 去执行登陆流程
    StoreProvider.of<TopState>(context)
        .dispatch(LoginAction(context, _userName, _password));
  }
}
