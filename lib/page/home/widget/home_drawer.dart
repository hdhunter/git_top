import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:git_top_flutter/common/config/config.dart';
import 'package:git_top_flutter/common/dao/issue_dao.dart';
import 'package:git_top_flutter/common/dao/repos_dao.dart';
import 'package:git_top_flutter/common/local/local_storage.dart';
import 'package:git_top_flutter/common/localization/default_localizations.dart';
import 'package:git_top_flutter/model/User.dart';
import 'package:git_top_flutter/redux/gsy_state.dart';
import 'package:git_top_flutter/redux/login_redux.dart';
import 'package:git_top_flutter/common/style/gsy_style.dart';
import 'package:git_top_flutter/common/utils/common_utils.dart';
import 'package:git_top_flutter/common/utils/navigator_utils.dart';
import 'package:git_top_flutter/widget/gsy_flex_button.dart';
import 'package:package_info/package_info.dart';
import 'package:redux/redux.dart';

/**
 * 主页drawer
 * Created by HunterZhang
 * Date: 2019-12-18
 */
class HomeDrawer extends StatelessWidget {
  showAboutDialog(BuildContext context, String versionName) {
    versionName ??= "Null";
    NavigatorUtils.showTopDialog(
        context: context,
        builder: (BuildContext context) => AboutDialog(
              applicationName: TopLocalizations.i18n(context).app_name,
              applicationVersion: TopLocalizations.i18n(context).app_version +
                  ": " +
                  versionName,
              applicationIcon: new Image(
                  image: new AssetImage(TopICons.DEFAULT_USER_ICON),
                  width: 50.0,
                  height: 50.0),
              applicationLegalese: "http://github.com/HDHunter",
            ));
  }

  showThemeDialog(BuildContext context, Store store) {
    List<String> list = [
      TopLocalizations.i18n(context).home_theme_default,
      TopLocalizations.i18n(context).home_theme_1,
      TopLocalizations.i18n(context).home_theme_2,
      TopLocalizations.i18n(context).home_theme_3,
      TopLocalizations.i18n(context).home_theme_4,
      TopLocalizations.i18n(context).home_theme_5,
      TopLocalizations.i18n(context).home_theme_6,
    ];
    CommonUtils.showCommitOptionDialog(context, list, (index) {
      CommonUtils.pushTheme(store, index);
      LocalStorage.save(Config.THEME_COLOR, index.toString());
    }, colorList: CommonUtils.getThemeListColor());
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: new StoreBuilder<TopState>(
        builder: (context, store) {
          User user = store.state.userInfo;
          return new Drawer(
            ///侧边栏按钮Drawer
            child: new Container(
              ///默认背景
              color: store.state.themeData.primaryColor,
              child: new SingleChildScrollView(
                ///item 背景
                child: Container(
                  constraints: new BoxConstraints(
                      minHeight: MediaQuery.of(context).size.height),
                  child: new Material(
                    color: TopColors.white,
                    child: new Column(
                      children: <Widget>[
                        new UserAccountsDrawerHeader(
                          //Material内置控件
                          accountName: new Text(
                            user.login ?? "---",
                            style: TopConstant.largeTextWhite,
                          ),
                          accountEmail: new Text(
                            user.email ?? user.name ?? "---",
                            style: TopConstant.normalTextLight,
                          ),
                          //用户名
                          //用户邮箱
                          currentAccountPicture: new GestureDetector(
                            //用户头像
                            onTap: () {},
                            child: new CircleAvatar(
                              //圆形图标控件
                              backgroundImage: new NetworkImage(
                                  user.avatar_url ??
                                      TopICons.DEFAULT_REMOTE_PIC),
                            ),
                          ),
                          decoration: new BoxDecoration(
                            //用一个BoxDecoration装饰器提供背景图片
                            color: store.state.themeData.primaryColor,
                          ),
                        ),
                        new ListTile(
                            title: new Text(
                              TopLocalizations.i18n(context).home_reply,
                              style: TopConstant.normalText,
                            ),
                            onTap: () {
                              String content = "";
                              CommonUtils.showEditDialog(
                                  context,
                                  TopLocalizations.i18n(context).home_reply,
                                  (title) {}, (res) {
                                content = res;
                              }, () {
                                if (content == null || content.length == 0) {
                                  return;
                                }
                                CommonUtils.showLoadingDialog(context);
                                IssueDao.createIssueDao(
                                    "CarGuo", "git_top_flutter", {
                                  "title":
                                      TopLocalizations.i18n(context).home_reply,
                                  "body": content
                                }).then((result) {
                                  Navigator.pop(context);
                                  Navigator.pop(context);
                                });
                              },
                                  titleController: new TextEditingController(),
                                  valueController: new TextEditingController(),
                                  needTitle: false);
                            }),
                        new ListTile(
                            title: new Text(
                              TopLocalizations.i18n(context).home_history,
                              style: TopConstant.normalText,
                            ),
                            onTap: () {
                              NavigatorUtils.gotoCommonList(
                                  context,
                                  TopLocalizations.i18n(context).home_history,
                                  "repositoryql",
                                  "history",
                                  userName: "",
                                  reposName: "");
                            }),
                        new ListTile(
                            title: new Hero(
                                tag: "home_user_info",
                                child: new Material(
                                    color: Colors.transparent,
                                    child: new Text(
                                      TopLocalizations.i18n(context)
                                          .home_user_info,
                                      style: TopConstant.normalTextBold,
                                    ))),
                            onTap: () {
                              NavigatorUtils.gotoUserProfileInfo(context);
                            }),
                        new ListTile(
                            title: new Text(
                              TopLocalizations.i18n(context).home_change_theme,
                              style: TopConstant.normalText,
                            ),
                            onTap: () {
                              showThemeDialog(context, store);
                            }),
                        new ListTile(
                            title: new Text(
                              TopLocalizations.i18n(context)
                                  .home_change_language,
                              style: TopConstant.normalText,
                            ),
                            onTap: () {
                              CommonUtils.showLanguageDialog(context);
                            }),
                        new ListTile(
                            title: new Text(
                              TopLocalizations.i18n(context).home_check_update,
                              style: TopConstant.normalText,
                            ),
                            onTap: () {
                              ReposDao.getNewsVersion(context, true);
                            }),
                        new ListTile(
                            title: new Text(
                              TopLocalizations.of(context)
                                  .currentLocalized
                                  .home_about,
                              style: TopConstant.normalText,
                            ),
                            onLongPress: (){
                              NavigatorUtils.goDebugDataPage(context);
                            },
                            onTap: () {
                              PackageInfo.fromPlatform().then((value) {
                                print(value);
                                showAboutDialog(context, value.version);
                              });
                            }),
                        new ListTile(
                            title: new TopFlexButton(
                              text: TopLocalizations.i18n(context).Login_out,
                              color: Colors.redAccent,
                              textColor: TopColors.textWhite,
                              onPress: () {
                                store.dispatch(LogoutAction(context));
                              },
                            ),
                            onTap: () {}),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
